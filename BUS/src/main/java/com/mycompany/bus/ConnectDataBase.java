package com.mycompany.bus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectDataBase {
    private static String databasename = new String("test");
    private static String DB_URL = "jdbc:sqlserver://localhost:1433;"
            + "databaseName=" + databasename + ";"
            + "integratedSecurity=true";
    private static String username = new String("sa");
    private static String password = new String("1212");

    public static void main(String[] args) {
        try {
            Connection connection = getConnection(DB_URL, username, password);

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("select * from *");
            while (resultSet.next()) {
                System.out.println("true");
            }

            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static Connection getConnection(String dbURL, String userName,
//                                           String password) {
//        Connection conn = null;
//        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            conn = DriverManager.getConnection(dbURL, userName, password);
//            System.out.println("connect successfully!");
//        } catch (Exception ex) {
//            System.out.println("connect failure!");
//            ex.printStackTrace();
//        }
//        return conn;
//    }
}
